package com.san.singtel.assignment.model.bird;

import com.san.singtel.assignment.model.Bird;
import lombok.Data;

/**
 * Created by sankarvinnakota on 23/09/18.
 */
@Data
public class Parrot extends Bird {

    public Parrot() {

    }
}
