package com.san.singtel.assignment.behaviour.sing;

/**
 * Created by sankarvinnakota on 23/09/18.
 */
public class DogSound extends CanSing {
    @Override
    public void sing() {
        System.out.println("Woof,woof");
    }
}
