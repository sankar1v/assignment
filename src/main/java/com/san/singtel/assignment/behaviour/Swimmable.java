package com.san.singtel.assignment.behaviour;

/**
 * Created by sankarvinnakota on 23/09/18.
 */
@FunctionalInterface
public interface Swimmable {
    void swim();
}
